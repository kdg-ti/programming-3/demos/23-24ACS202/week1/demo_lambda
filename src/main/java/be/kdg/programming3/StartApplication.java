package be.kdg.programming3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StartApplication {
    public static void calculator(MyFunctionalInterface lambda) {
        System.out.println("Calculating...");
        for (int i=0;i<10;i++) {
            System.out.println(lambda.calculate(i));
        }
        System.out.println("Finished!");
    }

    public static void main(String[] args) {
        calculator(number->Math.pow(10, number));
        calculator(number->number*number);
        calculator(number->number);
        //...
/*        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Lucas"));
        persons.add(new Person("Josefien"));
        persons.add(new Person("Matthijs"));
        persons.add(new Person("Rik"));
        persons.add(new Person("Roos"));
        Collections.sort(persons);
        System.out.println(persons);
        Collections.sort(persons, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().length() - o2.getName().length();
            }
        });
        Collections.sort(persons, (o1, o2) -> o1.getName().length() - o2.getName().length());
        System.out.println(persons);*/
    }
}
