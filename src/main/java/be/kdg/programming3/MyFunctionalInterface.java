package be.kdg.programming3;

@FunctionalInterface
public interface MyFunctionalInterface {
    double calculate(int value);
}
